# Website

## Introduction

This package includes the source code of the website https://meikellp.de/

## Requirements

* Node.js
* [Yarn](https://yarnpkg.com/)

## Getting started

### Start dev server

``` bash
yarn
yarn serve
```

### Build from source

``` bash
yarn
yarn build
```